---
title: Quiz FOR New Year 2022/23
date: 2020-12-22T18:42:58Z
style: |
  section.quiz {
    columns: 2 400px;
  }
  details {
    margin-bottom: 2em;
    break-inside: avoid;
  }
  summary {
      padding: 1em;
    background-color: #ccc;
    border-radius: 0.5em;
  }
  details[open] summary {
    background-color: pink;
  }
---

## Categories:

- geography
- English
- entertainment
- history
- art & literature
- science and nature
- sports and leisure

{{< quiz >}}